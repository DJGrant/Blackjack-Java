import java.util.ArrayList;

import mvc.AbstractModel;
import mvc.ModelEvent;

public class BlackjackModel extends AbstractModel {
	public boolean playerTurn = true;
	public boolean roundInProgress = true;
	
	public CardCollection deck = new CardCollection();
	public CardCollection playerHand = new CardCollection();
	public CardCollection dealerHand = new CardCollection();
	
	public int minBet = 100;
	
	public int playerCash = 1000;
	public int dealerCash = 1000;
	
	public int playerBet = 0;
	public int dealerBet = 0;
	
	public void dealPlayer() {
		dealCard(deck, playerHand, deck.cardCount()-1);
		
		if (playerHand.checkHandValue() > 21) {
			playerTurn = false;
			roundInProgress = false;
			
			dealerWins();
			
			ModelEvent me = new ModelEvent(this, 1, "Player busted!", 0);
			notifyChanged(me);
		}
	}
	public void dealDealer() {
		dealCard(deck, dealerHand, deck.cardCount()-1);
		
		if (dealerHand.checkHandValue() > 21) {
			playerTurn = false;
			roundInProgress = false;
			
			playerWins();
			
			ModelEvent me = new ModelEvent(this, 1, "Dealer busted!", 0);
			notifyChanged(me);
		}
	}
	
	public void dealCard(CardCollection dealer, CardCollection receiver, int index) {
		if (dealer.cardCount() > 0) {
			receiver.addCard(receiver.cardCount(), dealer.removeCard(index));
			
			ModelEvent me = new ModelEvent(this, 1, "", deck.cardCount());
			notifyChanged(me);
		}
	}
	
	public void startRound() {
		if (playerCash < minBet) {
			playerBet += playerCash;
			playerCash -= playerCash;
		} else {
			playerBet += minBet;
			playerCash -= minBet;
		}
		
		if (dealerCash < minBet) {
			dealerBet += dealerCash;
			dealerCash -= dealerCash;
		} else {
			dealerBet += minBet;
			dealerCash -= minBet;
		}
	}
	
	public void standButton() {
		if (playerTurn == true) {
			dealerTurn();
		} else {
			judgeHands();
		}
	}
	public void hitButton() {
		if (playerTurn == true) {
			dealPlayer();
		} else {
			dealDealer();
		}
	}
	public void doubleButton() {
		
		int pb = playerBet;
		int db = dealerBet;
		
		if (playerCash < playerBet) {
			playerBet += playerCash;
			playerCash -= playerCash;
		} else {
			playerBet += pb;
			playerCash -= pb;
		}
		if (dealerCash < dealerBet) {
			dealerBet += dealerCash;
			dealerCash -= dealerCash;
		} else {
			dealerBet += db;
			dealerCash -= db;
		}
		
		if (playerTurn == true) {
			dealPlayer();
			dealerTurn();
		} else {
			dealDealer();
			judgeHands();
		}
	}
	
	public void dealerTurn() {
		playerTurn = false;
		
		double val = Math.random() * 10;
		
		if (val < 5)
			standButton();
		else if (val >= 8.5)
			doubleButton();
		else {
			hitButton();
			standButton();
		}
	}
	
	public void judgeHands() {
		if (roundInProgress == true) {
			roundInProgress = false;
			
			int pVal = playerHand.checkHandValue(); // Player hand value
			int dVal = dealerHand.checkHandValue(); // Dealer hand value
			
			if (dVal > pVal) {
				//playerTurn = false;
				
				dealerWins();
				
				ModelEvent me = new ModelEvent(this, 1, "Dealer wins this round!", 0);
				notifyChanged(me);
			} else {
				//playerTurn = false;
				
				playerWins();
				
				ModelEvent me = new ModelEvent(this, 1, "Player wins this round!", 0);
				notifyChanged(me);
			}
		}
		//roundInProgress = true;
	}
	
	public void continueGame() {
		if (playerCash <= 0 && playerBet <= 0) {
			ModelEvent me = new ModelEvent(this, 1, "Player cash at zero. Dealer wins the entire game!", 0);
			notifyChanged(me);
		} else if (dealerCash <= 0 && dealerBet <= 0) {
			ModelEvent me = new ModelEvent(this, 1, "Dealer cash at zero. Player wins the entire game!", 0);
			notifyChanged(me);
		} else {
			playerTurn = true;
			roundInProgress = true;
			
			ModelEvent me = new ModelEvent(this, 1, "Player's Turn", 0);
			notifyChanged(me);
		}
	}
	
	public void playerWins() {
		playerCash = playerCash + playerBet + dealerBet;
		dealerBet = 0;
		playerBet = 0;
	}
	public void dealerWins() {
		dealerCash = dealerCash + dealerBet + playerBet;
		dealerBet = 0;
		playerBet = 0;
	}
	
	public void resetHands() {
		while (playerHand.cardCount() > 0) {
			dealCard(playerHand, deck, 0);
		}
		while (dealerHand.cardCount() > 0) {
			dealCard(dealerHand, deck, 0);
		}
		deck.shuffle();
		
		dealDealer();
		dealDealer();
		
		dealPlayer();
		dealPlayer();
		
		ModelEvent me = new ModelEvent(this, 1, "Reset Hands", 0);
		notifyChanged(me);
	}
	
	// ---
	
	public void instantiateDeck() {
		for (int i=1; i<=13; i++) {
			deck.addCard(new Card(i,"spades"));
			deck.addCard(new Card(i,"diamonds"));
			deck.addCard(new Card(i,"clubs"));
			deck.addCard(new Card(i,"hearts"));
		}
		deck.shuffle();
	}
}

class Card {
	public int value;
	public String suit;
	
	public Card() {
		value = 1;
		suit = "spades";
	}
	public Card(int v, String s) {
		value = v;
		suit = s;
	}
}

class CardCollection {
	ArrayList<Card> cards;
	
	public CardCollection() {
		cards = new ArrayList<Card>();
	}
	
	public void shuffle() {
		for (int i=0; i<cards.size(); i++) {
			int j = (int)(Math.random() * cards.size()-1);
			
			cards.add(j, cards.remove(i));
		}
	}
	
	public Card removeCard(int index) {
		return cards.remove(index);
	}
	
	public void addCard(Card element) {
		cards.add(element);
	}
	public void addCard(int index, Card element) {
		cards.add(index, element);
	}
	
	public int cardCount() {
		return cards.size();
	}
	
	public Card[] getCardsArray() {
		Card[] c = new Card[cards.size()];
		for (int i=0; i<cards.size(); i++) {
			c[i] = cards.get(i);
		}
		return c;
	}
	
	public int checkHandValue() {
		Card[] cards = this.getCardsArray();
		int total = 0;
		
		for (int i=0; i<cards.length; i++) {
			if (cards[i].value == 1) {
				if (total + 11 > 21)
					total += 1;
				else
					total += 11;
			} else if (cards[i].value > 10) {
				total += 10;
			} else {
				total += cards[i].value;
			}
		}
		
		return (total);
	}
	
}
