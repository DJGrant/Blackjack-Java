import mvc.AbstractController;
import mvc.JFrameView;

public class BlackjackController extends AbstractController {
	
	BlackjackModel CurrentModel;
	BlackjackView CurrentView;
	
    public BlackjackController(){
        setModel(new BlackjackModel());
		
		CurrentModel = ((BlackjackModel) getModel());
		CurrentView = ((BlackjackView) getView());
		
		CurrentModel.instantiateDeck();
		
		CurrentModel.dealDealer(); CurrentModel.dealDealer();
		CurrentModel.dealPlayer(); CurrentModel.dealPlayer();
		CurrentModel.startRound();
		
        setView(new BlackjackView((BlackjackModel)getModel(), this));
        ((JFrameView)getView()).setVisible(true);
    }

    public void operation (String option) {
		
		if ( option.equals("Stand") ) {
            CurrentModel.standButton();
			
        }
		if ( option.equals("Hit") ) {
            CurrentModel.hitButton();
			
        }
		if ( option.equals("Double") ) {
            CurrentModel.doubleButton();
			
        }
		
		else if ( option.equals("Continue") ) {
            CurrentModel.continueGame();
			
			if ( ( CurrentModel.playerCash > 0 || CurrentModel.playerBet > 0 ) &&
			( CurrentModel.dealerCash > 0 || CurrentModel.dealerBet > 0 ) ) {
				CurrentModel.startRound();
				CurrentModel.resetHands();
			}
        }
    }
}
