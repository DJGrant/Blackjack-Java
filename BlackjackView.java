import mvc.Controller;
import mvc.JFrameView;
import mvc.Model;
import mvc.ModelEvent;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import java.util.*;

public class BlackjackView extends JFrameView {
	
	public JLabel statusBar;
	
	public JLabel playerCashCounter;
	public JLabel dealerCashCounter;
	
	public JLabel playerBetCounter;
	public JLabel dealerBetCounter;
	
	public JPanel cardsPanel; // This holds both the player and dealer card panels.
	public JPanel playerCardsPanel; // Player Cards
	public JPanel dealerCardsPanel; // Dealer Cards
	
	public JPanel interfacePanel; // This holds the button panels
	public JPanel buttonPanel; // This holds the buttons for gameplay
	public JPanel continuePanel; // This holds the continue button
	public JPanel counterPanel; // This holds cash counter
	
	public HashMap<Integer, String> numberNames = new HashMap<Integer, String>();
	
	public BlackjackView(BlackjackModel model, BlackjackController controller) {
		
        super(model, controller);
		
		setCardMaps(); // We want the numbers of the cards to correspond to their names
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Stop the program when we close the window
		setLayout(new BorderLayout(10,10));
		
		setSize(640,480);
		
		statusBar = new JLabel("This took a lot longer than I thought it would. Anyway, you can start. It's your turn.");
		statusBar.setHorizontalAlignment(JLabel.CENTER);
		
		cardsPanel = new JPanel(); // This holds both the player and dealer card panels.
		cardsPanel.setLayout(new BoxLayout(cardsPanel, BoxLayout.PAGE_AXIS));
		playerCardsPanel = new JPanel(); // Player Cards
		dealerCardsPanel = new JPanel(); // Dealer Cards
		
		interfacePanel = new JPanel();
		interfacePanel.setLayout(new CardLayout());
		
		buttonPanel = new JPanel(); // This holds the buttons
		continuePanel = new JPanel();
		counterPanel = new JPanel(); // This holds the cash counters
		counterPanel.setLayout(new BorderLayout(10,10));
		counterPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		
        Handler handler = new Handler();
		
		JButton standButton = new JButton("Stand");
        standButton.addActionListener(handler);
		
		JButton hitButton = new JButton("Hit");
        hitButton.addActionListener(handler);
		
		JButton doubleButton = new JButton("Double");
        doubleButton.addActionListener(handler);
		
		JButton splitButton = new JButton("Split");
        splitButton.addActionListener(handler);
		
		JButton continueButton = new JButton("Continue");
        continueButton.addActionListener(handler);
		
		playerCashCounter = new JLabel("Player Cash: " + ((BlackjackModel) getModel()).playerCash);
		dealerCashCounter = new JLabel("Dealer Cash: " + ((BlackjackModel) getModel()).dealerCash);
		
		playerBetCounter = new JLabel("Player Bet: " + ((BlackjackModel) getModel()).playerBet);
		dealerBetCounter = new JLabel("Dealer Bet: " + ((BlackjackModel) getModel()).dealerBet);
		
		
		// Add stuff
		
		getContentPane().add(counterPanel, BorderLayout.PAGE_START); // Cash Counters
		counterPanel.add(playerCashCounter, BorderLayout.LINE_START);
		counterPanel.add(dealerCashCounter, BorderLayout.LINE_END);
		
		cardsPanel.add(Box.createRigidArea(new Dimension(0,25)));
		
		getContentPane().add(cardsPanel, BorderLayout.CENTER);
		
		cardsPanel.add(dealerCardsPanel, Component.CENTER_ALIGNMENT); // Dealer Cards
		
		cardsPanel.add(Box.createRigidArea(new Dimension(0,0)));
		cardsPanel.add(statusBar, Component.CENTER_ALIGNMENT);
		cardsPanel.add(Box.createRigidArea(new Dimension(0,10)));
		
		cardsPanel.add(playerCardsPanel, Component.CENTER_ALIGNMENT); // Player Cards
		
		
		
		getContentPane().add(interfacePanel, BorderLayout.PAGE_END);
		interfacePanel.add(buttonPanel, "Buttons");
		interfacePanel.add(continuePanel, "Continue");
		continuePanel.add(continueButton);
		
		buttonPanel.add(playerBetCounter);
		buttonPanel.add(standButton);
		buttonPanel.add(hitButton);
		buttonPanel.add(doubleButton);
		//buttonPanel.add(splitButton);
		buttonPanel.add(dealerBetCounter);
		
		drawDealerCards();
		drawPlayerCards();
		
		//pack();
    }
	
	public void drawPlayerCards() {
		drawCards(((BlackjackModel) getModel()).playerHand, playerCardsPanel, false);
	}
	
	public void drawDealerCards() {
		drawCards(((BlackjackModel) getModel()).dealerHand, dealerCardsPanel, true);
	}
	
	public void drawCards(CardCollection c, JPanel panel, boolean hideFirstCard) {
		panel.removeAll();
		
		//Card[] cards = ((BlackjackModel) getModel()).playerHand.getCardsArray();
		Card[] cards = c.getCardsArray();
		for (int i=0; i<cards.length; i++) {
			Card card = cards[i];
			ImageIcon icon;
			
			if (hideFirstCard == true && i == 0 && ((BlackjackModel) getModel()).roundInProgress == true)
				icon = createImageIcon("cards/mystery_card.png","A card");
			else
				icon = createImageIcon("cards/" + numberNames.get(card.value) + card.suit + ".png","A card");
			
			Image scaledCard = icon.getImage().getScaledInstance(88,128, Image.SCALE_SMOOTH);
			icon = new ImageIcon(scaledCard,"A card");
			JLabel imageLabel = new JLabel(icon);
			panel.add(imageLabel);
		}
	}
	
	@Override
    public void modelChanged(ModelEvent event) {
        String msg = event.getAmount() + "";
		
		if ( ((BlackjackModel) getModel()).playerTurn == false) {
			((CardLayout)interfacePanel.getLayout()).show(interfacePanel, "Continue");
		} else {
			((CardLayout)interfacePanel.getLayout()).show(interfacePanel, "Buttons");
		}
		
		if (!event.getActionCommand().equals("") && !event.getActionCommand().equals("Reset Hands"))
			statusBar.setText(event.getActionCommand());
		
		
		playerCashCounter.setText("Player Cash: " + ((BlackjackModel) getModel()).playerCash);
		dealerCashCounter.setText("Dealer Cash: " + ((BlackjackModel) getModel()).dealerCash);
		
		playerBetCounter.setText("Player Bet: " + ((BlackjackModel) getModel()).playerBet);
		dealerBetCounter.setText("Dealer Bet: " + ((BlackjackModel) getModel()).dealerBet);
		
		
		drawPlayerCards();
		drawDealerCards();
		
		getContentPane().revalidate();
		getContentPane().repaint();
    }


    class Handler implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            ((BlackjackController)getController()).operation(e.getActionCommand());
        }
    }
	
	/** Returns an ImageIcon, or null if the path was invalid. */
	protected ImageIcon createImageIcon(String path,
											   String description) {
		java.net.URL imgURL = getClass().getResource(path);
		if (imgURL != null) {
			return new ImageIcon(imgURL, description);
		} else {
			System.err.println("Couldn't find file: " + path);
			return null;
		}
	}
	
	private void setCardMaps() {
		numberNames.put(1, "ace_of_");
		numberNames.put(2, "2_of_");
		numberNames.put(3, "3_of_");
		numberNames.put(4, "4_of_");
		numberNames.put(5, "5_of_");
		numberNames.put(6, "6_of_");
		numberNames.put(7, "7_of_");
		numberNames.put(8, "8_of_");
		numberNames.put(9, "9_of_");
		numberNames.put(10, "10_of_");
		numberNames.put(11, "jack_of_");
		numberNames.put(12, "queen_of_");
		numberNames.put(13, "king_of_");
	}
}